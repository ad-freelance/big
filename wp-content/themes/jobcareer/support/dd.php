<?php
if (!function_exists('dd')) {
    function dd() {
        call_user_func_array('dump', func_get_args());
        die();
    }
}