<?php
require_once __DIR__ . '/../wp-load.php';

class JobAlert {

    protected $queueLists;
    protected $queueListIds = [];
    protected $templateDir = __DIR__ . '/../wp-content/themes/jobcareer/templates/email';

    public function __construct()
    {
        $this->queueLists = $this->getQueueLists();

        foreach ($this->queueLists as $key => $value) {
            $this->queueListIds[] = $value['job_id'];
        }
    }

    public function run()
    {
        echo "\n";
        echo "Queue run for job alert at: " . Date('Y-m-d H:i:s') . "\n";

        $users = $this->getUserList();
        $counter = 0;
        foreach ($users as $user) {
            $jobs = $this->getJobListByUser($user);
            if (!empty($jobs)) {
                $this->sendEmail($user, 'Lowongan Terbaru Untuk Anda', $jobs);
                $counter++;
            }
        }

        echo "$counter applicants alerted. \n";
        echo "Finished queue run for job alert at: " . Date('Y-m-d H:i:s') . "\n";
        echo "\n";

    }

    protected function getQueueLists()
    {
        global $wpdb;

        $queue = $wpdb->get_results("SELECT * FROM job_mail_queue WHERE status = 'WAITING'", ARRAY_A);
        return $queue;

    }

    protected function getUserList()
    {
        $args = [
            'role__in' => ['cs_candidate']
        ];

        return get_users($args);
    }

    protected function getJobListByUser($user)
    {
        $specialisms = get_user_meta($user->ID, 'cs_specialisms', true);

        $args = [
            'post_type' => 'jobs',
            'post__in' => $this->queueListIds,
            'tax_query' => [
                [
                    'taxonomy' => 'specialisms',
                    'field' => 'slug',
                    'terms' => $specialisms,
                ],
            ],
        ];

        $posts = (new WP_Query( $args ))->get_posts();
        wp_reset_postdata();
        return $posts;

    }

    protected function sendEmail($user, $subject, $jobLists)
    {
        $messageBody = $this->buildTemplate($user, $jobLists);
        echo "Sending to " . $user->user_email . "\n";
        wp_mail( $user->user_email, $subject, $messageBody, ['From:no-reply@big.co.id']);
    }

    protected function buildTemplate($user, $jobLists)
    {

        $mainTemplate = file_get_contents($this->templateDir . '/job-alert.html');
        $jobListTemplate = file_get_contents($this->templateDir . '/job-list.html');

        $content = '';

        foreach ($jobLists as $key => $job) {
            $cs_job_username = get_post_meta($job->ID, 'cs_job_username', true);
            // getting employer information
            $employer = get_userdata($cs_job_username);

            $jobVars = [
                'JOB_TITLE' => $job->post_title,
                'COMPANY_NAME' => $employer->display_name,
                'JOB_URL' => get_permalink($job)
            ];

            $jobContent = $jobListTemplate;
            foreach ($jobVars as $field => $value) {
                $jobContent = str_replace('[' .  $field .']', $value, $jobContent);
            }
            $content .= $jobContent;
        }

        $vars = [
            'JOB_LISTS' => $content,
            'USERNAME' => $user->display_name,
            'SEE_MORE_URL' => home_url(),
        ];

        foreach ($vars as $field => $value) {
            $mainTemplate = str_replace('[' . $field . ']', $value, $mainTemplate);
        }
        // dd(home_url());
        return $mainTemplate;

    }



}


$alerter = new JobAlert();
$alerter->run();
